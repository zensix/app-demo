# Docker

    docker build . -t stef/app-demo:v1
    docker run --name demo-app -p 3000:3000 stef/app-demo:v1

# docker-compose:
   
    docker-compose up -d  

# K8S
    docker tag stef/app-demo:v1 registry.stef.local:5000/stef/app-demo:v1
    docker push  registry.stef.local:5000/stef/app-demo:v1
    cd k8s
    kubectl create namespace app-demo
    kubectl apply -n app-demo -f all.yml

[![Build Status](http://192.168.88.13:8087/api/badges/stef/app_demo/status.svg)

