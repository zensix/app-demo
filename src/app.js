var createError = require('http-errors');
  express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  logger = require('morgan'),
  session = require('express-session'),
  fs = require('fs');

var passport = require('passport');
var Strategy = require('passport-local').Strategy;
global.__basedir = __dirname;
var AccountsModule= require('./lib/accounts');
accounts = new AccountsModule({filename:"accounts.json"});

accounts.findByUsername('admin',(err,admin) => {
  if(!admin){
    accounts.add({'username':'admin','password':'admin','role':'admin'},(err,user) => {
      console.log("user admin created.")
    })
  } else{
    console.log("user admin exist.")

  }
})

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var configRouter = require('./routes/config');

var apiRouter = require('./routes/api');


passport.use(new Strategy(
  function(username, password, cb) {
    accounts.findByUsername(username, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (!accounts.passwordVerified(user.id,password)) { return cb(null, false); }
      return cb(null, user);
    });
}));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});
  
passport.deserializeUser(function(id, cb) {
  accounts.findById(id, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  name: 'session-id',
  secret: '123-456-789',
  saveUninitialized: false,
  resave: false
}));



// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

// Load des configurations
var appconfig
console.log("load appconfig")
if (fs.existsSync('./config/app_config.json')) {
    console.log('./config/app_config.json'+' exist');
} else {
  fs.writeFileSync('./config/app_config.json', '{"AppName": "Demo app","Version": "1.1","Authenticate": true,"Type": "local"}', function (err) {
    if (err) return console.log(err);
    console.log('./config/app_config.json'+' initialized');
    
  });
}
appconfig = require('./config/app_config.json');
appconfig.MONGODB_USER= process.env.MONGODB_USER 
appconfig.MONGODB_PASSWORD= process.env.MONGODB_PASSWORD 
appconfig.DATABASE_SERVICE_NAME= process.env.DATABASE_SERVICE_NAME
appconfig.MONGODB_PORT= process.env.MONGODB_PORT 
appconfig.MONGODB_DATABASE= process.env.MONGODB_DATABASE 
// MongoDB
var mongoose = require('mongoose');
mongoConnectString="mongodb://"+appconfig.MONGODB_USER+":"+appconfig.MONGODB_PASSWORD+"@"+appconfig.DATABASE_SERVICE_NAME+":"+appconfig.MONGODB_PORT+"/"+appconfig.MONGODB_DATABASE
mongoose
    .connect(mongoConnectString, { useNewUrlParser: true,  useUnifiedTopology: true  , useCreateIndex: true, useFindAndModify:false})
    .then(console.log(`MongoDB connected ${mongoConnectString}`))
    .catch(err => console.log(err));


// Ajout des acces aux librairie additionnel npm
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));
app.use('/popper',express.static(__dirname +'/node_modules/popper.js/dist/umd'));

app.use('/bootstrap-icons.svg',express.static(__dirname +'/node_modules/bootstrap-icons/bootstrap-icons.svg'));
app.use('/icons',express.static(__dirname +'/node_modules/bootstrap-icons/icons'));

//const tokens = require("./middleware/custom.js");

app.use(accounts.checkToken.bind(accounts));
// Set var pass to router
app.set('appconfig',appconfig)
app.set('accounts',accounts)
app.set('dbcon',mongoose)



// Routage
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/config', configRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
