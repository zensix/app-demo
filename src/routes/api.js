// Sauvegarde de la configuration
const cookieParser = require('cookie-parser');
var express = require('express');
var router = express.Router();
const os = require('os');
const Message = require('../models/message')




router.use(function (req, res, next) {
    if ( !req.isAuthenticated() && !req.tokenVerified ) {
        if( !req.isAuthenticated() && req.path =='/' ){
            res.redirect('/login')
            return
        } else {
            res.json({"error":"Bad Api token"})
        }
    } else {
        next();
    }
});
// Generation de la page de configuration
router.get('/', function(req, res, next) {
    res.setHeader("x-api-key",req.user.token);
    res.render('api_home', { title: 'Api' ,user: req.user});
});

router.get('/status', function(req, res, next) {
    data={'status':'ko'}
    dbcon=req.app.get('dbcon')
    data.db=dbcon.connection.readyState;
    data.status="ok"
    res.json(data);
 });


router.get('/session', function(req, res, next) {
   res.json(req.session);
});



router.get('/header', function(req, res, next) {
    res.json(req.headers);
 });
 

router.get('/infos', function(req, res, next) {
    appconfig=req.app.get('appconfig')
    res.json(appconfig);
 });

 router.get('/accounts', function(req, res, next) {
    accounts=req.app.get('accounts')
    res.json(accounts.records);
 });

 
 router.get('/account/:id', function(req, res, next) {
    var id = req.params.id;
    accounts=req.app.get('accounts')
    res.json(accounts.records[+id-1]);
 });

 router.get('/osinfo',function(req, res, next){
    var data={"hostname":os.hostname(),"plateform":os.platform(),"release":os.release(),"uptime":os.uptime(),"totalmem":os.totalmem(),"freemem":os.freemem(),"Database": process.env.MONGO_URL,"network_interfaces":os.networkInterfaces()}
    res.json(data);
  });
  
  router.get('/messages',function(req, res, next){
    let mess = new Message({content:"Get Messages", crit:"Info"})
    mess.save(function() {
      Message.find({},function(err,docs){
       res.json(docs);
      })
    });
  });
  
  router.post('/message',function(req, res, next){
    let mess = new Message({content:req.body.content, crit:req.body.crit})
    mess.save(function(){
      Message.find({},function(err,docs){
        res.json(docs);
     })
    })
  });
  
  router.delete('/messages',function(req, res, next){
    let mess = new Message({content:req.body.content, crit:req.body.crit})
    mess.save(function(){
      Message.remove({},function(err,docs){
        res.json(docs);
     });
    });
  });

 
module.exports = router;
