// Sauvegarde de la configuration
var express = require('express');
var router = express.Router()
const fs = require('fs');


router.use(function (req, res, next) {
    if ( !req.isAuthenticated() ) {
        res.redirect('/login')
        return
    }
    next();
});
// Generation de la page de configuration
router.get('/', function(req, res, next) {
    res.render('app_config', { title: 'Configuration' ,icon:"sliders",user: req.user,appconfig:req.app.get("appconfig")});
  });
  
router.post('/', function(req, res, next) {
  req.app.set("appconfig",req.body)
  console.log(req.app.get('appconfig'))
  try{
    fs.writeFileSync('./config/app_config.json', JSON.stringify(req.app.get('appconfig'),null, 4))
    mongoConnectString="mongodb://"+appconfig.MONGODB_USER+":"+appconfig.MONGODB_PASSWORD+"@"+appconfig.DATABASE_SERVICE_NAME+":27017/"+appconfig.MONGODB_DATABASE
    dbcon=req.app.get('dbcon')
    dbcon
    .connect(mongoConnectString, { useNewUrlParser: true,  useUnifiedTopology: true  , useCreateIndex: true, useFindAndModify:false})
    .then(console.log(`MongoDB connected ${mongoConnectString}`))
    .catch(err => console.log(err));
    res.json({"status":"ok", message:"Config update"}) 

  } catch(err){
    res.json({"status":"err", message:err}) 
  }
 });
  
module.exports = router;
