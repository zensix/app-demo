const fs = require('fs')
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
const { createCipher } = require('crypto');
const { removeData } = require('jquery');


AccountsModule = function(initvar){
    this.internvar = initvar
    this.saltRounds = 10;
    this.internvar['recordpath']=__basedir+"/config/"+this.internvar.filename
    this.records=[]
    self=this
    try {
        if (fs.existsSync(self.internvar.recordpath)) {
          console.log("Check records ok")
          records_content = fs.readFileSync(this.internvar.recordpath);
          self.records = JSON.parse(records_content);
        }
    } catch(err) {
        fs.writeFileSync(self.internvar.recordpath, '[]', function (err) {
            if (err) return console.log(err);
            console.log(self.internvar.recordpath+' initialized');
            records_content = fs.readFileSync(self.internvar.recordpath);
            self.records = JSON.parse(records_content);
        });
    }
    this.version ='1.0'
}

AccountsModule.prototype.save = function(){
    self=this
    fs.writeFileSync(self.internvar.recordpath,JSON.stringify(self.records,null,2))
}
AccountsModule.prototype.get = function(item){
    return this.internvar[item]
}

AccountsModule.prototype.accounts = function() {
    self=this
    return self.records
}

AccountsModule.prototype.add =function(user, cb) {
    self=this
    process.nextTick(function() {
        var idx = self.records.length;
        user['id']=idx+1
        salt = bcrypt.genSaltSync(self.saltRounds);
        user['hashpassword'] = bcrypt.hashSync(user['password'],salt)
        delete user.password
        user['token']=uuidv4()
        self.records.push(user)
        self.save()
        cb(null,user['id']);
    });
}

AccountsModule.prototype.addifnotexist = function(user, cb) {
    self=this
    self.findByUsername(user.username,(err,result) => {
        if(result === null){
           console.log(err)
           self.add(user,cb)
        } else {
            cb(result)
        }
    })
}


AccountsModule.prototype.remove =function(id,cb){
    self=this
            try{
                self.records.splice(id-1,1)
                self.save()
                cb(null,`User id ${id} removed` )
            } catch (err){
                cb(err,null)
            }
        }

AccountsModule.prototype.dump= function(){
       console.log("dirname",__dirname )
       console.log(this.records)
}

AccountsModule.prototype.findById=function(id, cb) {
    self=this
    process.nextTick(function() {
        var idx = id - 1;
        if (self.records[idx]) {

            cb(null, {"id":self.records[idx].id,"username":self.records[idx].username,"role":self.records[idx].role,"token": self.records[idx].token});
        } else {
            cb(new Error('User ' + id + ' does not exist'));
        }
    });
}



AccountsModule.prototype.passwordVerified= function(id,password){
    self=this
    return bcrypt.compareSync(password,self.records[id-1].hashpassword);
}

AccountsModule.prototype.findByUsername= function(username, cb) {
    self=this
    process.nextTick(function() {
        for (var i = 0, len = self.records.length; i < len; i++) {
            var record = self.records[i];
            if (record.username === username) {
                return cb(null, record);
            }
        }
        return cb(null, null);
    });


}
AccountsModule.prototype.changePassword=function(id,oldpass,newpass,cb){
    self=this
    var user = self.records[id-1]
    if(self.passwordVerified(id,oldpass)){
        salt = bcrypt.genSaltSync(saltRounds);
        user.hashpassword = bcrypt.hashSync(newpass,salt)
        self.save()
        cb(null,user)
    } else {
        cb('bad password',user)
    }
}

AccountsModule.prototype.update=function(newdata,cb){
    self=this
    var user = self.records[newdata.id-1]
    if(newdata.hashpassword === user.hashpassword){
        self.records[newdata.id-1]=newdata
        self.save()
        cb(null,user)
    } else {
        cb('bad password',user)
    }
}

AccountsModule.prototype.attribut= function(id, attribut){
    self=this
    var user = self.records[id-1]
    return user[attribut]
}

AccountsModule.prototype.role= function(id){
    self=this
    return self.attribut(id,"role")
}

AccountsModule.prototype.checkToken =  function(req, res, next) {
    self=this
    var token=req.headers['x-api-key']
    req['tokenVerified']=false;

    try{
        user = self.records.find(el => el.token === token)
        if(user){
            req.tokenVerified=true
            req.apitoken=user.token
            req.session['userid']=user.id
        }

    } catch(err){
        console.log(err)
    }
    finally {
     next();
    }
}

module.exports = AccountsModule;